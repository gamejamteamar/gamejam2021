using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Model;
using UnityEngine;

public class CursorManager : MonoBehaviour
{
  public static CursorManager Instance;
  public Texture2D[] Textures;
  public bool IsItemCursor;

  public Texture2D Laser;
  public Texture2D Error;
  public Texture2D Arrow;

  private bool _changeLocked;

  void Start()
  {
    Instance = this;

    Reset();
  }

  public void SetError()
  {
    IsItemCursor = false;
    _changeLocked = true;
    StartCoroutine(ErrorBlink());
  }

  public void SetLaser()
  {
    IsItemCursor = false;

    if (_changeLocked)
      return;

    Cursor.SetCursor(Laser, Vector2.zero, CursorMode.ForceSoftware);
  }

  public void SetTexture(KeyType key)
  {
    Cursor.SetCursor(Textures[(int)key], Vector2.zero, CursorMode.ForceSoftware);
    IsItemCursor = true;
  }

  public void Reset()
  {
    IsItemCursor = false;

    if (_changeLocked)
      return;

    Cursor.SetCursor(Arrow, Vector2.zero, CursorMode.ForceSoftware);
  }

  private void OnDestroy()
  {
    Cursor.SetCursor(Arrow, Vector2.zero, CursorMode.ForceSoftware);
  }

  private IEnumerator ErrorBlink()
  {
    Cursor.SetCursor(Error, Vector2.zero, CursorMode.ForceSoftware);
    yield return new WaitForSeconds(0.5f);
    Cursor.SetCursor(Arrow, Vector2.zero, CursorMode.ForceSoftware);
    _changeLocked = false;
  }
}
