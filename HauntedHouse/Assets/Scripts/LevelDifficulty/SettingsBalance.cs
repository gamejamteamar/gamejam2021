using UnityEngine;

public class SettingsBalance : MonoBehaviour
{
  public float CatMaxSpeed;
  public float CatMinSpeed;
  public float CatDistanceToTriggerRunning;
  public float CatRunningSleepBarDamage;
  public float CatRunningSleepBarDamageTickPeriod;
  public float CatTimeToKillGhost;
  public float CatSleepBarDamageWhileKillingGhost;

  public float SpawnerMinTimeSec;
  public float SpawnerMaxTimeSec;
  public float SpawnerMinTimeLowerBorder;
  public float SpawnerMaxTimeLowerBorder;
  public float SpawnerDecreaseMinTimeSecStep;
  public float SpawnerDecreaseMaxTimeSecStep;
  public float SpawnerDecreaseTimeSecInterval;

  public float GhostDamageToHero;

  void Start()
  {

  }

  void Update()
  {

  }
}
