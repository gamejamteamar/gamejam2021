using System.Collections.Generic;
using UnityEngine;

public class DifficultManager : MonoBehaviour
{
  public int DifficultLevel; // 0 - easy, 1 - medium, 2 - hard

  private static DifficultManager difficultManager;

  void Awake()
  {
    DontDestroyOnLoad(gameObject);

    if (difficultManager == null)
    {
      difficultManager = this;
    }
    else
    {
      Object.Destroy(gameObject);
    }
  }
}
