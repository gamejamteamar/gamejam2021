using System.Collections.Generic;
using UnityEngine;

public class DifficultManagerSpawner : MonoBehaviour
{
  public int DifficultLevel; // 0 - easy, 1 - medium, 2 - hard

  public List<SettingsBalance> _settings;

  void Start()
  {
    var difficultManager = FindObjectOfType<DifficultManager>();
    var obj = Instantiate(_settings[difficultManager.DifficultLevel]);
    obj.gameObject.transform.SetSiblingIndex(0);
  }
}
