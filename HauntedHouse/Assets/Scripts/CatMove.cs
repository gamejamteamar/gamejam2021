using Assets.Scripts;
using Assets.Scripts.Model;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UI;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

public class CatMove : MonoBehaviour
{
  NavMeshAgent agent;

  private float MaxSpeed;
  private float MinSpeed;
  private float DistanceToTriggerRunning;
  public SleepBar SleepBar;

  private float SleepBarDamage;
  private float SleepBarDamageTickPeriod;

  private float TimeToKillGhost;
  private float SleepBarDamageWhileKillingGhost;

  private GameplaySoundManager _soundManager;

  public GameObject Line;

  public bool IgnorePortals;
  public bool InAttack;
  
  public Dictionary<KeyType, bool> inventory = new Dictionary<KeyType, bool>()
  {
    [KeyType.Newspapper] = false,
    [KeyType.Woods] = false,
    [KeyType.Glass] = false,
    [KeyType.Water] = false,
    [KeyType.Vinegar] = false,
    [KeyType.FreshAir] = false,
    [KeyType.Lid] = false,
    [KeyType.VacuumCleaner] = false
  };

  // null if nothing is chosen
  public KeyType? PickedKey { get; set; }
  
  public CatMovementMode CatMovementMode = CatMovementMode.CanMove;
  
  private List<GhostMovement> _ghostsToKill = new List<GhostMovement>();
  private List<ChallengeItem> _challengeItemList = new List<ChallengeItem>();

  private Animator _animator;
  private bool takingItem = false;

  public System.EventHandler OnResetInventoryState;
  public System.EventHandler OnResetInventoryWithoutMovementState;

  void Start()
  {
    agent = GetComponent<NavMeshAgent>();
    _animator = GetComponentInChildren<Animator>();
    _soundManager = FindObjectOfType<GameplaySoundManager>();

    var settings = FindObjectOfType<SettingsBalance>();
    MaxSpeed = settings.CatMaxSpeed;
    MinSpeed = settings.CatMinSpeed;
    DistanceToTriggerRunning = settings.CatDistanceToTriggerRunning;
    SleepBarDamage = settings.CatRunningSleepBarDamage;
    SleepBarDamageTickPeriod = settings.CatRunningSleepBarDamageTickPeriod;

    TimeToKillGhost = settings.CatTimeToKillGhost;
    SleepBarDamageWhileKillingGhost = settings.CatSleepBarDamageWhileKillingGhost;

    StartCoroutine("MakingNoise");
  }

  void Update()
  {
    if (CatMovementMode == CatMovementMode.CanMove)
    {
      if (Input.GetMouseButtonDown(0))
      {
        RaycastHit hit;        
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 1000, LayerMask.GetMask("Surface", "Trap")))
        {
          if (hit.collider.gameObject.layer == 10)
          {
            takingItem = true;
            GetComponentInChildren<CatRotation>().RotateCat(hit.collider.gameObject);
            return;
          }

          PointerEventData pointerData = new PointerEventData(EventSystem.current);
          pointerData.position = Input.mousePosition;

          List<RaycastResult> results = new List<RaycastResult>();
          EventSystem.current.RaycastAll(pointerData, results);

          for (int i = 0; i < results.Count; i++)
          {
            var inventoryUI = results[i].gameObject.GetComponent<InventoryUI>();
            var inventoryItem = results[i].gameObject.GetComponent<InventoryItem>();
            if (inventoryUI != null || inventoryItem != null)
              return;
          }

          GetComponentInChildren<CatRotation>().RotateCat(hit.point);
          agent.isStopped = false;
          agent.destination = hit.point;
        }

        var speed = GetSpeed(agent.remainingDistance);
        agent.speed = speed;
        SetAnimation(speed);
      }
      else if (Input.GetMouseButton(0) && !takingItem)
      {
        RaycastHit hit;
        
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 1000, LayerMask.GetMask("Surface")))
        {
          PointerEventData pointerData = new PointerEventData(EventSystem.current);
          pointerData.position = Input.mousePosition;

          List<RaycastResult> results = new List<RaycastResult>();
          EventSystem.current.RaycastAll(pointerData, results);

          for (int i = 0; i < results.Count; i++)
          {
            var inventoryUI = results[i].gameObject.GetComponent<InventoryUI>();
            var inventoryItem = results[i].gameObject.GetComponent<InventoryItem>();
            if (inventoryUI != null || inventoryItem != null)
              return;
          }

          GetComponentInChildren<CatRotation>().RotateCat(hit.point);
          agent.isStopped = false;
          agent.destination = hit.point;
        }

        var speed = GetSpeed(agent.remainingDistance);
        agent.speed = speed;
        SetAnimation(speed);
      }
      else if (Input.GetMouseButtonUp(0))
      {
        agent.isStopped = true;
        StopAnimation();
        takingItem = false;
      }
    }
    else if(CatMovementMode == CatMovementMode.UsingInventory)
    {
      StopAnimation();
      if (Input.GetMouseButtonDown(0))
      {
        if (_challengeItemList.Count > 0)
        {
          RaycastHit hit;
          if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 1000, LayerMask.GetMask("ChallengeItem")))
          {
            var challangeItem = _challengeItemList.FirstOrDefault(x => hit.collider.gameObject == x.gameObject);
            if (challangeItem != null)
            {
              if (PickedKey.HasValue && !challangeItem.WasActivated)
              {
                if (challangeItem.TryToUse(PickedKey.Value))
                {
                  inventory[PickedKey.Value] = false;
                  SleepBar.SetChallengeDone();
                }
              }
            }
          }
        }

        OnResetInventoryState(this, null);
      }
    }
    else if (CatMovementMode == CatMovementMode.KillingGhost)
    {
    }
  }

  private void StopAnimation()
  {
    _animator.SetBool("Walking", false);
    _animator.SetBool("Running", false);
    _soundManager.StopCatRun();
    _soundManager.StopCatRunFast();
  }

  private void SetAnimation(float speed)
  {
    if (speed <= MinSpeed)
    {
      _animator.SetBool("Walking", true);
      _animator.SetBool("Running", false);
      _soundManager.StopCatRunFast();
      _soundManager.PlayCatRun();
    }
    else
    {
      _animator.SetBool("Walking", false);
      _animator.SetBool("Running", true);
      _soundManager.StopCatRun();
      _soundManager.PlayCatRunFast();
    }
  }

  private float GetSpeed(float remainingDistance)
  {
    if (remainingDistance > DistanceToTriggerRunning)
      return MaxSpeed;

    return MinSpeed;
  }

  public void CatchGhost(GhostMovement ghost)
  {
    StartCoroutine(KillGhost(ghost));
  }

  IEnumerator KillGhost(GhostMovement ghost)
  {
    InAttack = true;
    
    OnResetInventoryWithoutMovementState(this, null);

    CatMovementMode = CatMovementMode.KillingGhost;
    agent.isStopped = true;

    _ghostsToKill.Add(ghost);

    GetComponentInChildren<CatRotation>().RotateCat(ghost.gameObject);

    StopAnimation();

    var attackType = Random.Range(0, 2);
    Debug.Log("Attack number: " + attackType);
    if(attackType == 0)
      _animator.SetBool("IsAttack", true);
    else
      _animator.SetBool("IsAttack2", true);

    ghost.StopMovement();
    SleepBar.IsRecovering = false;
    
    _soundManager.PlayCatScream();

    yield return new WaitForSeconds(0.5f);
    SleepBar.TakeDamage(SleepBarDamageWhileKillingGhost);
    yield return new WaitForSeconds(TimeToKillGhost - 0.5f);    

    _ghostsToKill.Remove(ghost);

    ghost.animator.SetBool("IsDying", true);
    
    if (_ghostsToKill.Count == 0)
    {
      SleepBar.IsRecovering = true;
      CatMovementMode = CatMovementMode.CanMove;
      _animator.SetBool("IsAttack", false);
      _animator.SetBool("IsAttack2", false);
      InAttack = false;
    }

    ghost.gameObject.GetComponent<BoxCollider>().enabled = false;
    ghost.gameObject.GetComponent<NavMeshAgent>().enabled = false;
    yield return new WaitForSeconds(1.5f);
    Destroy(ghost.gameObject);    
  }

  public void StopMovement()
  {
    agent.isStopped = true;
  }

  IEnumerator MakingNoise()
  {
    while (true)
    {
      if (!agent.isStopped && agent.speed > MinSpeed)
      {
        SleepBar.IsRecovering = false;
        SleepBar.TakeDamage(SleepBarDamage);
      }
      else
      {
        SleepBar.IsRecovering = true;
      }

      yield return new WaitForSeconds(SleepBarDamageTickPeriod);
    }
  }

  public bool AddToInventory(KeyType keyType)
  {
    _animator.SetTrigger("Take");
    Debug.Log("Key solver added " + keyType.ToString());
    inventory[keyType] = true;
    return true;
  }

  public void SetRotation()
  {
    GetComponentInChildren<CatRotation>().RotateToCamera();
  }

  private void OnTriggerEnter(Collider other)
  {
    var challenge = other.GetComponent<ChallengeItem>();
    if (challenge != null)
      _challengeItemList.Add(challenge);
  }

  private void OnTriggerExit(Collider other)
  {
    var challenge = other.GetComponent<ChallengeItem>();
    if (challenge != null)
      _challengeItemList.Remove(challenge);
  }
}
