using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;

public enum NavigationSide
{
    Middle,
    Left,
    Top,
    Right,
    Bottom
}

public class NavigationTarget : MonoBehaviour, IPointerEnterHandler
{
    public event Action<NavigationSide> onSideEnter;

    public NavigationSide side;

    public void OnPointerEnter(PointerEventData eventData)
    {
        onSideEnter?.Invoke(side);
    }
}
