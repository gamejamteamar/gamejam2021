using System;
using Cinemachine;
using UnityEngine;

public class NavigationRoot : MonoBehaviour
{
    public NavigationTarget[] targets;

    public CinemachineVirtualCamera catCamera;
    public CinemachineVirtualCamera freeCamera;

    public Transform freeLookPoint;
    public Transform startLookPoint;
    public int maxLookingDistance = 50;
    public float stepSize = 0.5f;

    private NavigationSide _movingSide;

    private void OnEnable()
    {
        foreach (var t in targets)
            t.onSideEnter += HandleNewSide;
    }

    private void OnDisable()
    {
        foreach (var t in targets)
            t.onSideEnter -= HandleNewSide;
    }

    private void Update()
    {
        if (_movingSide == NavigationSide.Middle)
            return;

        var position = freeLookPoint.transform.position;
        if (Vector3.Distance(position, startLookPoint.position) >= maxLookingDistance)
            return;

        if (_movingSide == NavigationSide.Left)
            position.x -= stepSize;
        if (_movingSide == NavigationSide.Right)
            position.x += stepSize;
        if (_movingSide == NavigationSide.Top)
            position.y += stepSize;
        if (_movingSide == NavigationSide.Bottom)
            position.y -= stepSize;

        freeLookPoint.transform.position = position;
    }

    private void HandleNewSide(NavigationSide side)
    {
        Debug.Log($"Move to {side}");
        _movingSide = side;
        var stopped = _movingSide == NavigationSide.Middle;
        catCamera.gameObject.SetActive(stopped);
        freeCamera.gameObject.SetActive(!stopped);

        if (stopped)
        {
            freeLookPoint.position = startLookPoint.position;
        }
    }
}
