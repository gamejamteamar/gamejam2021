using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class GhostMovement : MonoBehaviour
{
  public static System.Random rand = new System.Random();
  public Animator animator;

  NavMeshAgent agent;

  private Quaternion? rotateTo;

  void Start()
  {
    agent = GetComponent<NavMeshAgent>();
    animator = GetComponent<Animator>();
  }

  void Update()
  {
    if (rotateTo != null)
    {
      transform.rotation = Quaternion.Slerp(transform.rotation, rotateTo.Value, Time.deltaTime * 5f);
      transform.rotation = new Quaternion(0, transform.rotation.y, 0, transform.rotation.w);
    }
  }

  private void OnTriggerEnter(Collider other)
  {
    var catMove = other.GetComponent<CatMove>();
    if (catMove != null)
    {
      CatCollide(catMove);
      return;
    }

    var hero = other.GetComponent<SleepingHero>();
    if (hero != null)
    {
      HeroCollide(hero);
      return;
    }

    var point = other.GetComponent<PathPart>();
    if (point == null || point.NextPoints == null || point.NextPoints.Length == 0)
      return;

    var nextPointIndex = rand.Next(0, point.NextPoints.Length);
    agent.destination = point.NextPoints[nextPointIndex].transform.position;
  }

  private void CatCollide(CatMove catMove)
  {
    var direction = (catMove.transform.position - transform.position).normalized;
    var lookRotation = Quaternion.LookRotation(direction);
    rotateTo = lookRotation;

    animator.SetBool("IsAttack", true);
    catMove.CatchGhost(this);
  }

  private void HeroCollide(SleepingHero hero)
  {
    var direction = (hero.transform.position - transform.position).normalized;
    var lookRotation = Quaternion.LookRotation(direction);
    rotateTo = lookRotation;

    StopMovement();
    animator.SetBool("IsAttack", true);
    StartCoroutine(KillHero(hero));
  }

  private IEnumerator KillHero(SleepingHero hero)
  {
    while (true)
    {
      hero.DieHard();
      yield return new WaitForSeconds(1);
    }
  }

  public void StopMovement()
  {
    agent.isStopped = true;
  }
}
