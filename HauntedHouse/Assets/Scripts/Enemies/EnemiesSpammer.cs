using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UI;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemiesSpammer : MonoBehaviour
{
  [SerializeField] private GameObject _ghostSpawnVfx;

  public List<Transform> targets;
  public GameObject[] enemies;

  private float MinTimeSec;
  private float MaxTimeSec;

  private float MinTimeLowerBorder;
  private float MaxTimeLowerBorder;

  private float DecreaseMinTimeSecStep;
  private float DecreaseMaxTimeSecStep;
  private float DecreaseTimeSecInterval;

  public int ActiveEnemiesCount = 0;

  private readonly List<GameObject> _lastEnemies = new List<GameObject>();
  private readonly List<Transform> _lastTargets = new List<Transform>();

  private GameplaySoundManager _soundManager;

  public void StopSpam(GameObject spamPoint)
  {
    targets.Remove(spamPoint.transform);
    Destroy(spamPoint);
  }

  private void Start()
  {
    StartCoroutine(Spam());
    StartCoroutine(DecreaseTime());

    _soundManager = FindObjectOfType<GameplaySoundManager>();

    var settings = FindObjectOfType<SettingsBalance>();
    MinTimeSec = settings.SpawnerMinTimeSec;
    MaxTimeSec = settings.SpawnerMaxTimeSec;
    MinTimeLowerBorder = settings.SpawnerMinTimeLowerBorder;
    MaxTimeLowerBorder = settings.SpawnerMaxTimeLowerBorder;
    DecreaseMinTimeSecStep = settings.SpawnerDecreaseMinTimeSecStep;
    DecreaseMaxTimeSecStep = settings.SpawnerDecreaseMaxTimeSecStep;
    DecreaseTimeSecInterval = settings.SpawnerDecreaseTimeSecInterval;
  }

  private IEnumerator Spam()
  {
    var done = false;
    while (!done)
    {
      var time = Random.Range(MinTimeSec, MaxTimeSec);
      yield return new WaitForSeconds(time);

      var enemy = NextEnemy();
      var target = NextTarget();

      done = enemy == null || target == null;

      var vfx = Instantiate(_ghostSpawnVfx, target.position, Quaternion.identity);
      Destroy(vfx, 1f);

      var go = Instantiate(enemy, target.position, Quaternion.identity);
      _soundManager.PlayGhostSpawn();
      var subscriber = go.AddComponent<EnemyDeadSubscriber>();
      subscriber.Dead += OnDead;
      ActiveEnemiesCount++;
    }
  }

  private void OnDead(EnemyDeadSubscriber enemy)
  {
    enemy.Dead -= OnDead;
    ActiveEnemiesCount--;
  }

  private GameObject NextEnemy()
  {
    if (enemies.Length == 1)
      return enemies.First();

    if (_lastEnemies.Count >= enemies.Length)
      _lastEnemies.Clear();

    while (true)
    {
      var rand = Random.Range(0, enemies.Length);
      var enemy = enemies[rand];
      if (_lastEnemies.Contains(enemy))
        continue;

      _lastEnemies.Add(enemy);
      return enemy;
    }
  }

  private Transform NextTarget()
  {
    if (targets.Count == 1)
      return targets.First();

    if (_lastTargets.Count >= targets.Count)
      _lastTargets.Clear();

    while (true)
    {
      var rand = Random.Range(0, targets.Count);
      var target = targets[rand];
      if (_lastTargets.Contains(target))
        continue;

      _lastTargets.Add(target);
      return target;
    }    
  }

  IEnumerator DecreaseTime()
  {
    while (true)
    {
      yield return new WaitForSeconds(DecreaseTimeSecInterval);

      if (MinTimeSec > MinTimeLowerBorder)
        MinTimeSec -= DecreaseMinTimeSecStep;

      if (MaxTimeSec > MaxTimeLowerBorder)
        MaxTimeSec -= DecreaseMaxTimeSecStep;
    }
  }
}
