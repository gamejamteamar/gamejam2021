using System;
using UnityEngine;

public class EnemyDeadSubscriber : MonoBehaviour
{
    public event Action<EnemyDeadSubscriber> Dead;

    private void OnDestroy()
    {
        Dead?.Invoke(this);
    }
}
