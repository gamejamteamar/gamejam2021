using Assets.Scripts.Model;
using Assets.Scripts.Analytics;
using UnityEngine;

public class WinManager : MonoBehaviour
{
  public int completedChallenges;
  public int totalChallenges = 8;

  public void Win()
  {
    FindObjectOfType<UIManager>().OpenWinScene();
    
    Tracker.LogLevelWin(GetDifficulty());
  }

  public void Lose(LoseType loseType)
  {
    var uiManager = FindObjectOfType<UIManager>();
    
    if (loseType == LoseType.GhostComing)
      uiManager.OpenLoseGhosts();
    else
      uiManager.OpenLoseWokeUp();
    
    Tracker.LogLevelLose(GetDifficulty(), loseType);
  }

  private Difficulty GetDifficulty()
  {
    var manager = FindObjectOfType<DifficultManager>();
    if (manager.DifficultLevel == 0)
      return Difficulty.Easy;

    if (manager.DifficultLevel == 1)
      return Difficulty.Normal;

    return Difficulty.Hard;
  }

  public void SetChallengeDone()
  {
    completedChallenges++;

    if (completedChallenges == totalChallenges)
      Win();
  }
}
