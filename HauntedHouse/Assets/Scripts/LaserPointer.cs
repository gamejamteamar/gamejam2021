using UnityEngine;

namespace Assets.Scripts
{
    public class LaserPointer : MonoBehaviour
    {
        private LineRenderer _lineRenderer;

        void Start()
        {
            _lineRenderer = GetComponent<LineRenderer>();
            _lineRenderer.useWorldSpace = true;
        }

        void Update()
        {
            if (Input.GetMouseButtonDown(0) || Input.GetMouseButton(0))
            {
                CursorManager.Instance.SetLaser();
                _lineRenderer.enabled = true;
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                var isHitSomething = Physics.Raycast(ray, out var hit, 1000, LayerMask.GetMask("Surface"));

                if (!isHitSomething)
                {
                    _lineRenderer.SetPosition(0,  _lineRenderer.transform.position);
                    _lineRenderer.SetPosition(1,  ray.GetPoint(10f));
                    return;
                }

                _lineRenderer.SetPosition(0,  _lineRenderer.transform.position);
                _lineRenderer.SetPosition(1,  hit.point);
            }
            else if (Input.GetMouseButtonUp(0))
            {
                _lineRenderer.enabled = false;
                if (!CursorManager.Instance.IsItemCursor)
                    CursorManager.Instance.Reset();
            }
        }
    }
}