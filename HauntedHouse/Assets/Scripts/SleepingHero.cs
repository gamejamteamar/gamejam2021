using Assets.Scripts;
using UnityEngine;

public class SleepingHero : MonoBehaviour
{
  public SleepBar SleepBar;
  private float DamagePerOneGhost;

  private void Start()
  {
    var settings = FindObjectOfType<SettingsBalance>();
    DamagePerOneGhost = settings.GhostDamageToHero;
  }

  public void DieHard()
  {
    SleepBar.TakeDamage(DamagePerOneGhost);
  }
}
