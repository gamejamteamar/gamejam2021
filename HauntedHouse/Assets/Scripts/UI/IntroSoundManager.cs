using UnityEngine;

namespace UI
{
    public class IntroSoundManager : MonoBehaviour
    {
        public AudioSource Night;
        public AudioSource Sleep;
        public AudioSource Ghosts;
        
        public void PlayGhosts() => 
            Ghosts.Play();
    }
}