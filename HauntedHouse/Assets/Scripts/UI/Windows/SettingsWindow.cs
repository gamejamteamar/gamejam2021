using UnityEngine;

namespace UI
{
    public class SettingsWindow : WindowBase
    {
        private MainMenuSoundManager _audio;

        private void Start()
        {
            _audio = FindObjectOfType<MainMenuSoundManager>();
        }
        public void OnCloseClick()
        {
            _audio.PlayClick();
            UIManager.OpenMainScene();
        }
    }
}