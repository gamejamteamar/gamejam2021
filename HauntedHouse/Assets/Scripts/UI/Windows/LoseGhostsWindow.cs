using Assets.Scripts.Analytics;
using UnityEngine;

namespace UI
{
    public class LoseGhostsWindow : WindowBase
    {
        public void OnReplay()
        {
            FindObjectOfType<LoseScreenSoundManager>().PlayClick();
            UIManager.OpenGameScene();
            
            Tracker.LogLevelReplay();
        }

        public void OnMenu()
        {
            FindObjectOfType<LoseScreenSoundManager>().PlayClick();
            UIManager.OpenMainScene();
        }
    }
}