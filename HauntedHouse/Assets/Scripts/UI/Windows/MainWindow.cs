using Assets.Scripts.Analytics;

namespace UI
{
    public class MainWindow : WindowBase
    {
        private MainMenuSoundManager _audio;

        private void Start()
        {
            _audio = FindObjectOfType<MainMenuSoundManager>();
        }

        public void Easy()
        {
            FindObjectOfType<DifficultManager>().DifficultLevel = 0;
            UIManager.OpenGameScene();
            _audio.PlayClick();
            
            Tracker.LogLevelStart(Difficulty.Easy);
        }
        
        public void Normal()
        {
            FindObjectOfType<DifficultManager>().DifficultLevel = 1;
            UIManager.OpenGameScene();
            _audio.PlayClick();
            
            Tracker.LogLevelStart(Difficulty.Normal);
        }
        
        public void Hard()
        {
            FindObjectOfType<DifficultManager>().DifficultLevel = 2;
            UIManager.OpenGameScene();
            _audio.PlayClick();
            
            Tracker.LogLevelStart(Difficulty.Hard);
        }
    }
}