using UnityEngine;

namespace UI
{
    public class WinWindow : WindowBase
    {
        public void OnReplayClick()
        {
            FindObjectOfType<WinScreenSoundManager>().PlayClick();
            UIManager.OpenGameScene();
        }

        public void OnMainMenuClick()
        {
            FindObjectOfType<WinScreenSoundManager>().PlayClick();
            UIManager.OpenMainScene();
        }

        public void OnExitClick()
        {
            FindObjectOfType<WinScreenSoundManager>().PlayClick();
            Application.Quit();
        }
    }
}