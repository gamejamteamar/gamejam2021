using UnityEngine;

namespace UI
{
    public class WindowBase : MonoBehaviour
    {
        protected UIManager UIManager;
        public void Initialize(UIManager uiManager) => 
            UIManager = uiManager;
    }
}