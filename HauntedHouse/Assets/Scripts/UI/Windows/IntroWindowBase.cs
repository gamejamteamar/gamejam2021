using System;
using Cinemachine;
using UnityEngine;

namespace UI
{
    public class IntroWindowBase : WindowBase
    {
        [SerializeField] private int slideIndex;
        [SerializeField] private bool isLast;
        
        private CinemachineVirtualCamera _virtualCamera;

        private bool _skipFirstSlide = true;
        
        private void Start()
        {
            _virtualCamera = FindObjectOfType<CinemachineVirtualCamera>();
        }

        private void Update()
        {
            if (slideIndex == 1 && _skipFirstSlide)
            {
                ToNextPoint();
                _skipFirstSlide = false;
            }
        }

        public void ToNextPoint()
        {
            if (isLast)
            {
                UIManager.OpenGameScene();
                return;
            }
            
            UIManager.OpenSlide(slideIndex + 1);
            _virtualCamera.GetCinemachineComponent<CinemachineTrackedDolly>().m_PathPosition++;
        }
    }
}