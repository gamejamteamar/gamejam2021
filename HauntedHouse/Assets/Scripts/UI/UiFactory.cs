using System;
using Assets.Scripts.Analytics;
using UnityEngine;

namespace UI
{
    public class UiFactory : MonoBehaviour
    {
        [SerializeField] private GameObject uiManager;
        [SerializeField] private int _state;

        private void Start()
        {
            var obj = FindObjectOfType<UIManager>();
            
            var manager = obj == null
                ? Instantiate(uiManager, Vector3.zero, Quaternion.identity).GetComponent<UIManager>()
                : obj;
            
            switch (_state)
            {
                case 1:
                    manager.GetComponent<UIManager>().OpenMainScene();
                    break;
                case 2:
                    manager.GetComponent<UIManager>().OpenGameScene();
                    break;
                case 3:
                    manager.GetComponent<UIManager>().OpenWinScene();
                    break;
                case 4:
                    manager.GetComponent<UIManager>().OpenSlide(1);
                    break;
                case 5:
                    manager.GetComponent<UIManager>().OpenLoseGhosts();
                    break;
                case 6:
                    manager.GetComponent<UIManager>().OpenLoseWokeUp();
                    break;
            }
        }
    }
}