using System;
using DigitalRuby.Tween;
using UnityEngine;
using Random = UnityEngine.Random;

namespace UI
{
    public class ObjectTween : MonoBehaviour
    {
        private FloatTween _tween;

        private void Start()
        {
            Appear();
        }

        [ContextMenu("Appear")]
        public void Appear()
        {
            gameObject.transform.localScale = Vector3.zero;
            
            Action<ITween<float>> updateScale = tween =>
            {
                transform.localScale = new Vector3(tween.CurrentValue, tween.CurrentValue, tween.CurrentValue);
            };

            var cubicEaseOut = TweenScaleFunctions.QuarticEaseInOut;
            _tween = gameObject.Tween(Random.value.ToString(), 0f, 1f, 1.7f, cubicEaseOut, x => updateScale(x), tween => Disappear());
        }
        
        [ContextMenu("Disappear")]
        public void Disappear()
        {
            if (_tween.State == TweenState.Running)
                _tween.Stop(TweenStopBehavior.DoNotModify);
            
            Action<ITween<float>> updateScale = tween =>
            {
                transform.localScale = new Vector3(tween.CurrentValue, tween.CurrentValue, tween.CurrentValue);
            };

            var cubicEaseOut = TweenScaleFunctions.QuarticEaseInOut;
            _tween = gameObject.Tween("test", transform.localScale.x, 0f, 1f, cubicEaseOut, x => updateScale(x), tween => Destroy(transform.gameObject));
        }
    }
}