using UnityEngine;

namespace UI
{
  public class GameplaySoundManager : MonoBehaviour
  {
    public AudioSource MainTheme;
    public AudioSource Click;
    public AudioSource ItemFound;
    public AudioSource CatScream;
    public AudioSource CatRun;
    public AudioSource GhostSpawn;
    public AudioSource CatRunFast;

    private void Start()
    {
      MainTheme.Play();
    }

    public void PlayClick()
    {
      Click.Play();
    }

    public void PlayItemFound()
    {
      ItemFound.PlayDelayed(0.5f);
    }

    public void PlayCatScream()
    {
      CatScream.Play();
    }

    public void PlayCatRun()
    {
      if (!CatRun.isPlaying)
        CatRun.Play();
    }

    public void StopCatRunAll()
    {
      CatRun.Stop();
      CatRunFast.Stop();
    }

    public void StopCatRun()
    {
      CatRun.Stop();
    }

    public void StopCatRunFast()
    {
      CatRunFast.Stop();
    }

    public void PlayGhostSpawn()
    {
      GhostSpawn.Play();
    }

    public void PlayCatRunFast()
    {
      if(!CatRunFast.isPlaying)
        CatRunFast.Play();
    }
  }
}