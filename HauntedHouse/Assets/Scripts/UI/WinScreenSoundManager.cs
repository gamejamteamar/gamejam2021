using System;
using UnityEngine;

namespace UI
{
    public class WinScreenSoundManager : MonoBehaviour
    {
        public AudioSource MainTheme;
        public AudioSource Click;
        public AudioSource Purring;
        
        public void PlayClick() => 
            Click.Play();
    }
}