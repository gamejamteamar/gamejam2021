using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Model;
using UnityEngine;

namespace UI
{
    public class ObjectPickedVfx : MonoBehaviour
    {
        [SerializeField] private ObjectTween _tween;
        [SerializeField] private List<GameObject> _keysByIndex;

        public void InitializeAndStart(KeyType type)
        {
            _keysByIndex.ElementAt((int) type).SetActive(true);
            _tween.Appear();
        }
    }
}