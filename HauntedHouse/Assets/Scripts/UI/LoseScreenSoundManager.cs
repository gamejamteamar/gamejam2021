using UnityEngine;

namespace UI
{
    public class LoseScreenSoundManager : MonoBehaviour
    {
        public AudioSource MainTheme;
        public AudioSource Click;

        private void Start()
        {
            MainTheme.PlayDelayed(1.5f);
        }

        public void PlayClick() => 
            Click.Play();
    }
}