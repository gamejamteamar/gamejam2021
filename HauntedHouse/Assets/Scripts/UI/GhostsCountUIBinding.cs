using System;
using TMPro;
using UnityEngine;

namespace UI
{
    public class GhostsCountUIBinding : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _ghostsCountUI;
        private EnemiesSpammer _spawner;

        private void Start()
        {
            _spawner = FindObjectOfType<EnemiesSpammer>();
            if (_spawner == null)
                throw new ArgumentException("Couldn't find spammer");
        }

        private void Update()
        {
            _ghostsCountUI.text = _spawner.ActiveEnemiesCount.ToString();
        }
    }
}