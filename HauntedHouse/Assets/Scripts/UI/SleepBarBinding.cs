using System;
using System.Collections;
using System.Linq;
using Assets.Scripts;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI
{
  public class SleepBarBinding : MonoBehaviour
  {
    [SerializeField] private Slider _slider;
    [SerializeField] private GameObject _redSlider;

    private SleepBar _bar;
    private void Start()
    {
      _bar = SceneManager
          .GetActiveScene()
          .GetRootGameObjects()
          .ToList()
          .FirstOrDefault(x => x.GetComponent<SleepBar>() != null)
          ?.GetComponent<SleepBar>();

      if (_bar == null)
        throw new ArgumentException("Couldn't find SleepBar!");

      _slider.maxValue = _bar.MaxSleepScale;
    }

    private void Update()
    {
      if (_bar != null)
      {
        _redSlider.GetComponent<Slider>().value = _bar.SleepScale;
        _slider.value = _bar.SleepScale;
      }
    }

    public void ShowRed()
    {
      StartCoroutine(ShowRedForOneSec());
    }

    IEnumerator ShowRedForOneSec()
    {
      _redSlider.SetActive(true);
      yield return new WaitForSeconds(0.5f);
      _redSlider.SetActive(false);
    }
  }
}