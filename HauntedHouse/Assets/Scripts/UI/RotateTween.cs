using DigitalRuby.Tween;
using UnityEngine;

namespace UI
{
    public class RotateTween : MonoBehaviour
    {
        private void Start()
        {
            void Rotate(ITween<float> tween)
            {
                Debug.Log($"Tween - {tween.CurrentValue}");
                transform.RotateAround(transform.position, Vector3.up, tween.CurrentValue);
            }

            var cubicEaseOut = TweenScaleFunctions.QuarticEaseInOut;
            gameObject.Tween("test2", 2f, 270f, 1.7f, cubicEaseOut, Rotate);
        }
    }
}