using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Model;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
  public class InventoryItem : MonoBehaviour
  {
    [SerializeField] private List<GameObject> _skinsByIndex;
    [SerializeField] private GameObject _selectedGlow;
    [SerializeField] private GameObject _itemPickedVfx;

    private CatMove _catMove;
    public KeyType Type { get; private set; }

    public bool IsOwned { get; private set; }
    private Color _gray;
    private Color _normalColor;
    private Image _skin;

    private bool _isPicked = false;
    private VfxSpawnPoint _vfxSpawnPoint;

    private GameplaySoundManager _soundManager;

    private void Start()
    {
      _soundManager = FindObjectOfType<GameplaySoundManager>();
    }

    public void Initialize(KeyType type, bool owned, CatMove cat, VfxSpawnPoint vfxSpawnPoint)
    {
      _catMove = cat;
      Type = type;
      IsOwned = owned;
      _vfxSpawnPoint = vfxSpawnPoint;
      _catMove.OnResetInventoryState += (s, e) => ResetState();
      _catMove.OnResetInventoryWithoutMovementState += (s, e) => ResetStateWithoutMovement();

      _skin = _skinsByIndex.ElementAt((int)type).GetComponent<Image>();
      _gray = new Color(_skin.color.r, _skin.color.g, _skin.color.b, 0.0f);

      _normalColor = _skin.color;
      _skin.gameObject.SetActive(true);
    }

    public void OnClick()
    {
      _soundManager.PlayClick();
      if (_catMove.PickedKey != null || !IsOwned)
        return;

      _catMove.CatMovementMode = CatMovementMode.UsingInventory;
      _catMove.PickedKey = Type;
      _catMove.Line.SetActive(true);
      _isPicked = true;
      _selectedGlow.SetActive(true);
      CursorManager.Instance.SetTexture(Type);
    }

    public void Update()
    {
      _skin.color = IsOwned ? _normalColor : _gray;

      if (!Input.GetMouseButtonDown(1) || !_isPicked)
        return;

      ResetState();
    }

    void ResetState()
    {
      StartCoroutine(ResetStateWait());
    }

    void ResetStateWithoutMovement()
    {
      _isPicked = false;
      _catMove.PickedKey = null;
      CursorManager.Instance.Reset();
      _selectedGlow.SetActive(false);
    }

    IEnumerator ResetStateWait()
    {
      yield return new WaitForSeconds(0.1f);
      _catMove.Line.SetActive(false);
      _isPicked = false;
      _catMove.CatMovementMode = CatMovementMode.CanMove;
      _catMove.PickedKey = null;
      CursorManager.Instance.Reset();
      _selectedGlow.SetActive(false);
      _soundManager.PlayClick();
    }

    public void UpdateIsOwned(bool value)
    {
      if (value && !IsOwned)
      {
        SpawnVfx();
        _soundManager.PlayItemFound();
      }

      IsOwned = value;
    }

    private void SpawnVfx()
    {
      var @object = Instantiate(_itemPickedVfx, _vfxSpawnPoint.transform.position, Quaternion.identity, _vfxSpawnPoint.transform);
      @object.GetComponent<ObjectPickedVfx>().InitializeAndStart(Type);
    }
  }
}