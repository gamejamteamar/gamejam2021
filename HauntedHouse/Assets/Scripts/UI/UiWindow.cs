
namespace UI
{
    public enum UiWindow
    {
        Main = 1,
        Settings = 2,
        GameHud = 3,
        Win = 4,
        Slide1 = 5,
        Slide2 = 6,
        Slide3 = 7,
        LoseGhosts = 8,
        LoseWokeUp = 9,
    }
}
