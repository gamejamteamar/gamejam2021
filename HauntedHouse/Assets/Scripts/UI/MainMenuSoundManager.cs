using UnityEngine;

namespace UI
{
    public class MainMenuSoundManager : MonoBehaviour
    {
        public AudioSource MainTheme;
        public AudioSource Click;
        public AudioSource Keyboard;
        
        private void Start()
        {
            MainTheme.Play();
        }

        public void PlayClick()
        {
            Click.Play();
        }

        public void PlayKeyboard()
        {
            Keyboard.Play();
        }

        public void StopKeyboard()
        {
            Keyboard.Stop();
        }
    }
}