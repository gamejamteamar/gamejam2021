using System;
using System.Collections.Generic;
using UnityEngine;

namespace UI
{
    public class InventoryUI : MonoBehaviour
    {
        [SerializeField] private Transform _parent;
        [SerializeField] private GameObject _inventoryItemTemplate;

        private readonly List<InventoryItem> _items = new List<InventoryItem>();
        private CatMove _cat;
        private VfxSpawnPoint _vfxSpawnPoint;

        private void Start()
        {
            var catMove = FindObjectOfType<CatMove>();
            if (catMove == null)
                throw new ArgumentException("Cat not found");

            _cat = catMove;
            _vfxSpawnPoint = FindObjectOfType<VfxSpawnPoint>();
            
            foreach (var kvp in catMove.inventory)
            {
                var obj = Instantiate(_inventoryItemTemplate, _parent);
                var inventoryItem = obj.GetComponent<InventoryItem>();
                inventoryItem.Initialize(kvp.Key, kvp.Value, catMove, _vfxSpawnPoint);
                _items.Add(inventoryItem);
            }
        }

        private void Update()
        {
            foreach (var item in _items)
            {
                item.UpdateIsOwned(_cat.inventory[item.Type]);
            }
        }
    }
}