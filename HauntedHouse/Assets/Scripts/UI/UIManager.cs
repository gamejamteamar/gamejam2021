using System;
using UI;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Canvas))]
public class UIManager : MonoBehaviour
{
    [SerializeField] private GameObject _mainWindow;
    [SerializeField] private GameObject _settings;
    [SerializeField] private GameObject _gameHud;
    [SerializeField] private GameObject _winWindow;
    [SerializeField] private GameObject _loseGhostsWindow;
    [SerializeField] private GameObject _loseWokeUpWindow;
    
    [SerializeField] private GameObject _slide1;
    [SerializeField] private GameObject _slide2;
    [SerializeField] private GameObject _slide3;
    
    private const string HouseSceneName = "House";
    private const string MenuSceneName = "Menu";
    private const string IntroSceneName = "Intro";
    private const string WinSceneName = "Win";
    private const string LoseGhostsSceneName = "LoseGhosts";
    private const string LoseWokeUpSceneName = "LoseWokeUp";

    private OpenedWindow _openedWindow;
    private Transform _parent;

    private void Awake() => 
        _parent = GetComponent<Canvas>().transform;

    public void OpenGameScene() => 
        OpenSceneAsync(HouseSceneName, then: OpenGameHud);
    
    public void OpenSettings()
    {
        DestroyCurrentIfExists();
        _openedWindow = OpenedWindow.CreateSettings(this,TryInstantiate(_settings, _parent));
    }

    public void OpenMainScene()
    {
        OpenSceneAsync(MenuSceneName, then: OpenMain);
    }

    public void OpenWinScene()
    {
        OpenSceneAsync(WinSceneName, then: OpenWin);
    }

    public void OpenIntro()
    {
        OpenSceneAsync(IntroSceneName, () => OpenSlide(2));
    }

    public void OpenSlide(int index)
    {
        DestroyCurrentIfExists();
        
        if (index == 1)
            _openedWindow = OpenedWindow.CreateSlide1(this, TryInstantiate(_slide1, _parent));
        
        if (index == 2)
            _openedWindow = OpenedWindow.CreateSlide2(this, TryInstantiate(_slide2, _parent));
        
        if (index == 3)
            _openedWindow = OpenedWindow.CreateSlide3(this, TryInstantiate(_slide3, _parent));
    }

    public void OpenLoseGhosts()
    {
        OpenSceneAsync(LoseGhostsSceneName, then: OpenGhostsWindow);
    }

    public void OpenLoseWokeUp()
    {
        OpenSceneAsync(LoseWokeUpSceneName, then: OpenLoseWokeUpWindow);
    }

    private void OpenLoseWokeUpWindow()
    {
        DestroyCurrentIfExists();
        _openedWindow = OpenedWindow.CreateLoseWokeUp(this, TryInstantiate(_loseWokeUpWindow, _parent));
    }

    private void OpenGhostsWindow()
    {
        DestroyCurrentIfExists();
        _openedWindow = OpenedWindow.CreateLoseGhosts(this, TryInstantiate(_loseGhostsWindow, _parent));
    }

    private void OpenWokeUpWindow()
    {
        DestroyCurrentIfExists();
        _openedWindow = OpenedWindow.CreateLoseWokeUp(this, TryInstantiate(_loseWokeUpWindow, _parent));
    }
    
    private void OpenWin()
    {
        DestroyCurrentIfExists();
        _openedWindow = OpenedWindow.CreateWin(this, TryInstantiate(_winWindow, _parent));
    }

    private void OpenMain()
    {
        DestroyCurrentIfExists();
        _openedWindow = OpenedWindow.CreateMain(this, TryInstantiate(_mainWindow, _parent));
    }

    private GameObject TryInstantiate(GameObject window, Transform parent) => 
        !parent ? null : Instantiate(window, parent);

    private void OpenGameHud()
    {
        DestroyCurrentIfExists();
        
        _openedWindow = OpenedWindow.CreateGameHud(this, TryInstantiate(_gameHud, _parent));
    }

    private void OpenSceneAsync(string sceneName, Action then)
    {
        if (SceneManager.GetActiveScene().name == sceneName)
        {
            then?.Invoke();
            return;
        }

        var operation = SceneManager.LoadSceneAsync(sceneName);
        operation.completed += ExecuteThan;

        void ExecuteThan(AsyncOperation op)
        {
            then?.Invoke();
            operation.completed -= ExecuteThan;
        }
    }

    private void DestroyCurrentIfExists()
    {
        if (_openedWindow?.Instance != null)
            Destroy(_openedWindow.Instance);
    }
}
