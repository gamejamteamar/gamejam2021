using UnityEngine;

namespace UI
{
    public class OpenedWindow
    {
        public UiWindow Id { get; set; }
        public GameObject Instance { get; }

        private OpenedWindow(GameObject instance, UiWindow id, UIManager uiManager)
        {
            Instance = instance;
            Id = id;
            
            if (Instance)
                Instance.GetComponent<WindowBase>().Initialize(uiManager);
        }

        public static OpenedWindow CreateSettings(UIManager manager, GameObject obj)
        {
            var settings = new OpenedWindow(obj, UiWindow.Settings, manager);
            return settings;
        }

        public static OpenedWindow CreateMain(UIManager manager, GameObject obj)
        {
            var main = new OpenedWindow(obj, UiWindow.Main, manager);
            return main;
        }
        
        public static OpenedWindow CreateGameHud(UIManager manager, GameObject obj)
        {
            var hud = new OpenedWindow(obj, UiWindow.GameHud, manager);
            return hud;
        }

        public static OpenedWindow CreateWin(UIManager manager, GameObject obj)
        {
            var win = new OpenedWindow(obj, UiWindow.Win, manager);
            return win;
        }

        public static OpenedWindow CreateSlide1(UIManager manager, GameObject obj)
        {
            var slide = new OpenedWindow(obj, UiWindow.Slide1, manager);
            return slide;
        }
        
        public static OpenedWindow CreateSlide2(UIManager manager, GameObject obj)
        {
            var slide = new OpenedWindow(obj, UiWindow.Slide2, manager);
            return slide;
        }
        
        public static OpenedWindow CreateSlide3(UIManager manager, GameObject obj)
        {
            var slide = new OpenedWindow(obj, UiWindow.Slide3, manager);
            return slide;
        }

        public static OpenedWindow CreateLoseGhosts(UIManager manager, GameObject obj)
        {
            var window = new OpenedWindow(obj, UiWindow.LoseGhosts, manager);
            return window;
        }
        
        public static OpenedWindow CreateLoseWokeUp(UIManager manager, GameObject obj)
        {
            var window = new OpenedWindow(obj, UiWindow.LoseWokeUp, manager);
            return window;
        }
    }
}