using UnityEngine;

namespace UI
{
    public class TypingAtDeskEventRecevier : MonoBehaviour
    {
        public void Play()
        {
            FindObjectOfType<MainMenuSoundManager>().PlayKeyboard();
        }
        
        public void Stop()
        {
            FindObjectOfType<MainMenuSoundManager>().StopKeyboard();
        }
    }
}