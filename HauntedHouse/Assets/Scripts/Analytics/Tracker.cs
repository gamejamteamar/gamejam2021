using System;
using System.Threading.Tasks;
using Assets.Scripts.Model;
using Firebase;
using Firebase.Analytics;
using Firebase.Extensions;
using UnityEngine;
#if UNITY_IOS
// Include the IosSupport namespace if running on iOS:
using Unity.Advertisement.IosSupport;
#endif

namespace Assets.Scripts.Analytics
{
  public enum Difficulty
  {
    Easy,
    Normal,
    Hard
  }

  public enum LevelResult
  {
    Win,
    Loose,
  }

  public class Tracker
  {
    private static string _userId;
    private static bool _inited;
    private static FirebaseApp _app;

    private static Parameter _parameterLevel;
    public static void LogStart() => Log("app_start", new Parameter("userId", _userId));
    
    public static void LogLevelStart(Difficulty difficulty, int level = 1)
    {
      _parameterLevel = new Parameter(FirebaseAnalytics.ParameterLevelName, $"Level-{level}-{difficulty}".ToLower());
      Log(FirebaseAnalytics.EventLevelStart, _parameterLevel, new Parameter("replay", "false"));
    }

    public static void LogLevelReplay()
    {
      Log(FirebaseAnalytics.EventLevelStart, _parameterLevel, new Parameter("replay", "true"));
    }
    
    public static void LogLevelWin(Difficulty difficulty, int level = 1) => 
      Log(FirebaseAnalytics.EventLevelEnd, 
        new Parameter(FirebaseAnalytics.ParameterSuccess, "true"), 
        new Parameter("level", $"{level}"), 
        new Parameter("difficulty", $"{difficulty}".ToLower()), 
        new Parameter(FirebaseAnalytics.ParameterLevelName, $"Level-{level}-{difficulty}".ToLower()));
    
    public static void LogLevelLose(Difficulty difficulty, LoseType looseType, int level = 1) => 
      Log(FirebaseAnalytics.EventLevelEnd, 
        new Parameter(FirebaseAnalytics.ParameterSuccess, $"false"), 
        new Parameter("loose_type", $"{looseType}".ToLower()), 
        new Parameter("level", $"{level}"), 
        new Parameter(FirebaseAnalytics.ParameterLevelName, $"Level-{level}-{difficulty}".ToLower()));

    public static void Init()
    {
#if UNITY_IOS
      // Check the user's consent status.
      // If the status is undetermined, display the request request: 
      if (ATTrackingStatusBinding.GetAuthorizationTrackingStatus() ==
          ATTrackingStatusBinding.AuthorizationTrackingStatus.NOT_DETERMINED)
      {
        ATTrackingStatusBinding.RequestAuthorizationTracking();
      }
#endif
      Debug.Log("FirebaseApp initializing...");

      if (!PlayerPrefs.HasKey("userUniqId"))
        PlayerPrefs.SetString("userUniqId", Guid.NewGuid().ToString());

      _userId = PlayerPrefs.GetString("userUniqId");

      Debug.Log($"FirebaseApp user {_userId}");

      FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task =>
      {
        var dependencyStatus = task.Result;
        if (dependencyStatus == DependencyStatus.Available)
        {
          InitializeFirebase();
          LogStart();
        }
        else
        {
          Debug.LogError(
            "Could not resolve all Firebase dependencies: " + dependencyStatus);
        }
      });
    }

    private static void InitializeFirebase()
    {
      Debug.Log("Enabling data collection.");
      FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);

      Debug.Log("Set user properties.");
      // Set the user's sign up method.
      FirebaseAnalytics.SetUserProperty(
        FirebaseAnalytics.UserPropertySignUpMethod,
        "Google");
      // Set the user ID.
      FirebaseAnalytics.SetUserId(_userId);
      // Set default session duration values.
      FirebaseAnalytics.SetSessionTimeoutDuration(new TimeSpan(0, 30, 0));
      _inited = true;
    }

    private static void Log(string name, params Parameter[] parameters)
    {
      if (!_inited)
      {
        Debug.LogWarning("Analytics is not inited");
        return;
      }

      Debug.LogWarning($"Tracker. Send event {name}");
      FirebaseAnalytics.LogEvent(name, parameters);
    }
  }
}