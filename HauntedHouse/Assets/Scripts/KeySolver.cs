using Assets.Scripts.Model;
using UnityEngine;

public class KeySolver : MonoBehaviour
{
  public KeyType keyType;
  private CatMove _catNear;

  void Start()
  {

  }

  void Update()
  {
    if (Input.GetMouseButtonDown(0) && _catNear != null)
    {
      RaycastHit hit;
      if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 1000, LayerMask.GetMask("Trap")))
      {
        if(hit.collider.gameObject == gameObject)
        {
          if(_catNear.AddToInventory(keyType))
            Destroy(gameObject);
        }
      }
    }
  }

  private void OnTriggerEnter(Collider other)
  {
    var cat = other.GetComponent<CatMove>();
    if (cat != null)
      _catNear = cat;
  }

  private void OnTriggerExit(Collider other)
  {
    var cat = other.GetComponent<CatMove>();
    if (cat != null)
      _catNear = null;
  }
}
