using UnityEngine;

public class CatRotation : MonoBehaviour
{
  private Quaternion? rotateTo;

  void Start()
  {

  }

  void Update()
  {
    if(rotateTo != null)
    {
      transform.rotation = Quaternion.Slerp(transform.rotation, rotateTo.Value, Time.deltaTime * 5f);
      transform.rotation = new Quaternion(0, transform.rotation.y, 0, transform.rotation.w);
    }
  }

  public void RotateCat(GameObject gameObjectTo)
  {
    var direction = (gameObjectTo.transform.position - transform.position).normalized;
    var lookRotation = Quaternion.LookRotation(direction);

    rotateTo = lookRotation;
  }

  public void RotateCat(Vector3 positionTo)
  {
    var direction = (positionTo - transform.position).normalized;
    var lookRotation = Quaternion.LookRotation(direction);

    rotateTo = lookRotation;
  }

  public void RotateToCamera()
  {
    transform.rotation = new Quaternion(transform.rotation.x, 180, transform.rotation.z, transform.rotation.w);
  }
}
