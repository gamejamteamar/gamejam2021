using System.Collections;
using Assets.Scripts.Model;
using UnityEngine;

public class ChallengeItem : MonoBehaviour
{
  public KeyType targetKey;

  public GameObject inactive;
  public GameObject inprogress;
  public GameObject active;

  public GameObject spamPoint;

  public EnemiesSpammer spammer;

  public bool WasActivated;

  public bool TryToUse(KeyType key)
  {
    if (targetKey != key)
    {
      CursorManager.Instance.SetError();
      return false;
    }

    AnimateActivation();

    //StartCoroutine(AnimateActivation());

    return true;
  }

  private void AnimateActivation()
  {
    inactive.SetActive(false);
    inprogress.SetActive(true);

    // yield return new WaitForSeconds(0.1f);

    //inprogress.SetActive(false);
    active.SetActive(true);

    WasActivated = true;
    spammer.StopSpam(spamPoint);
  }
}
