using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class Portal : MonoBehaviour
{
  public Transform target;
  private CatMove _catNear;

  void Update()
  {
    if (_catNear != null && Input.GetMouseButton(0))
    {
      if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out var hit, 1000, LayerMask.GetMask("Teleport")))
      {
        if (hit.collider.gameObject == gameObject)
          TeleportCat();
      }
    }
  }

  private void TeleportCat()
  {
    if (_catNear.IgnorePortals || _catNear.InAttack)
      return;

    var n = _catNear.gameObject.GetComponent<NavMeshAgent>();
    n.enabled = false;
    var targetPosition = target.position;
    _catNear.transform.position = new Vector3(targetPosition.x, targetPosition.y, targetPosition.z);
    _catNear.SetRotation();
    n.enabled = true;
    _catNear.IgnorePortals = true;
    
    StartCoroutine(EnablePortals());
  }
  
  private IEnumerator EnablePortals(float sec = 2)
  {
    var cat = _catNear;
   
    yield return new WaitForSeconds(sec);
    cat.IgnorePortals = false;

    _catNear = null;
  }

  private void OnTriggerEnter(Collider other)
  {
    if (other.CompareTag("Cat"))
    {
      _catNear = other.GetComponent<CatMove>();
      TeleportCat();
    }

    if (other.CompareTag("Enemies"))
    {
      var n = other.gameObject.GetComponent<NavMeshAgent>();
      n.enabled = false;
      var targetPosition = target.position;
      other.transform.position = new Vector3(targetPosition.x, targetPosition.y, targetPosition.z);
      n.enabled = true;
    }
  }

  private void OnTriggerExit(Collider other)
  {
    if (other.CompareTag("Cat"))
    {
      var cat = other.GetComponent<CatMove>();
      if (cat != null)
        _catNear = null;
    }
  }
}
