﻿using Assets.Scripts.Model;
using System.Collections;
using UI;
using UnityEngine;

namespace Assets.Scripts
{
  public class SleepBar: MonoBehaviour
  {
    public WinManager WinManager;
    public float SleepScale { get; private set; }

    public float MaxSleepScale;
    public float SleepRecoveringStep;
    public float SleepRecoveringTickPreiod;

    public bool IsRecovering = true;

    SleepBarBinding sleepBarBinding;
    private bool _losed;

    void Start()
    {
      SleepScale = 100;
      _losed = false;
      StartCoroutine("Sleeping");
    }

    void Update()
    {
      if(sleepBarBinding == null)
      {
        sleepBarBinding = FindObjectOfType<SleepBarBinding>();
      }
    }

    public void StartSleeping()
    {

    }

    IEnumerator Sleeping()
    {
      while(true)
      {
        if (IsRecovering)
        {
          if (SleepScale < MaxSleepScale)
            SleepScale += SleepRecoveringStep;
        }

        Debug.Log(SleepScale);
        yield return new WaitForSeconds(SleepRecoveringTickPreiod);
      }
    }

    public void TakeDamage(float damage)
    {
      if(sleepBarBinding != null)
        sleepBarBinding.ShowRed();

      if (SleepScale <= damage)
        SleepScale = 0;
      else
        SleepScale -= damage;

      if (SleepScale == 0 && !_losed)
      {
        _losed = true;
        Lose();
      }
    }

    public void Lose()
    {
      WinManager.Lose(LoseType.SleepBarEnded);
    }

    public void SetChallengeDone()
    {
      WinManager.SetChallengeDone();
    }
  }
}
