﻿namespace Assets.Scripts.Model
{
  public enum CatMovementMode
  {
    CanMove = 0,
    UsingInventory = 1,
    KillingGhost = 2
  }
}
