﻿namespace Assets.Scripts.Model
{
  public enum KeyType
  {
    Newspapper = 0,
    Woods = 1,
    Glass = 2,
    Water = 3,
    Vinegar = 4,
    FreshAir = 5,
    Lid = 6,
    VacuumCleaner = 7
  }
}
