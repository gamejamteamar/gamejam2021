#if UNITY_IOS
using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;

public static class PostProcessBuildTasks
{
  private const string kXcodeTargetName = "Unity-iPhone";

  [PostProcessBuild( 1000 )]
  static void OnPostprocessBuild(BuildTarget buildTarget, string path)
  {
    UpdateXcodeProject( path );
  }
 
  private static void UpdateXcodeProject ( string path )
  {
    // Load
    string projectPath = Path.Combine( path, kXcodeTargetName + ".xcodeproj/project.pbxproj" );
    var pbxProject = new PBXProject();
    pbxProject.ReadFromFile( projectPath );
 
    string targetMain = pbxProject.GetUnityMainTargetGuid();
 
    // Remove OTHER_LDFLAGS from main target
    // Example of one of the multiple messages that appears at the Xcode console:
    // Class FIRAAdExposureReporter is implemented in both .../FirebaseTest.app/FirebaseTest
    // and .../FirebaseTest.app/Frameworks/UnityFramework.framework/UnityFramework.
    // One of the two will be used. Which one is undefined.
 
    foreach( var buildConfigName in pbxProject.BuildConfigNames() )
    {
      string configGuid = pbxProject.BuildConfigByName( targetMain, buildConfigName );
      pbxProject.SetBuildPropertyForConfig( configGuid, "OTHER_LDFLAGS", string.Empty );
    }
    // Save
    pbxProject.WriteToFile( projectPath );
  }
}
#endif